<?php

class Vds_Controller_Item extends Controller_Auth_Website {

	protected $form = array(
		'id'			=> '',
		'name'			=> '',
		'msrp'			=> '',
		'value'			=> '',
		'starting_bid'	=> '',
		'reserve_price'	=> '',
	);

	public function action_add()
	{
		$auction_id = $this->request->param('id');
		$auction = Auction::instance(ORM::factory('Auction', $auction_id));

		$validation = Validation::factory($this->request->post())
								->rule('auction_id', 'not_empty')
								->rule('name', 'not_empty')
								->rule('msrp', 'not_empty')
								->rule('value', 'not_empty')
								->rule('donor-name', 'not_empty')
								->rule('type', 'not_empty');

		if ($validation->check())
		{	
			$auction = Auction::instance(ORM::factory('Auction', $this->request->post('auction_id')));
			
			$orm_contact = ORM::factory('Contact')
									->where('name','=', $this->request->post('donor-name'))
									->find();

			if ($orm_contact->loaded())
			{
				$contact = Contact::instance($orm_contact);
			}
			else 
			{
				if ($this->request->post('donor-name') != '')
				{
					$contact = Contact::create()
									->name($this->request->post('donor-name'))
									->save();
				}
				else
				{
					$orm_contact = ORM::factory('Contact')->where('name','=','Anonymous')->find();
					$contact = Contact::instance($orm_contact);
				}
			}

			$item = Item::create()
							->auction($auction)
							->name($this->request->post('name'))
							->msrp($this->request->post('msrp'))
							->value($this->request->post('value'))
							->type($this->request->post('type'))
							->contact($contact);

			if ($this->request->post('starting_bid'))
			{
				$item->starting_bid($this->request->post('starting_bid'));
			}

			if ($this->request->post('reserve_price'))
			{
				$item->reserve_price($this->request->post('reserve_price'));
			}

			$item->save();

			$this->view = $item->as_array();
			$this->view['contact'] = $item->contact()->as_array();
		}
		else
		{
			$errors = $validation->errors();
			$this->view = $errors;
		}
	}

}