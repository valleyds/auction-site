<?php

class Vds_Controller_Dashboard extends Controller_Auth_Website {

	public function action_index()
	{
		$auction_id = 1;
		$auction = Auction::instance(ORM::factory('Auction', $auction_id));
		$this->title = 'Dashboard';
		$this->view = View::factory('dashboard')
							->set('auction', $auction);
	}

}