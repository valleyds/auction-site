<?php defined('SYSPATH') or die('No direct script access.');

class Vds_Controller_Index extends Controller_Website {

	public function action_index()
	{
		if (Auth::instance()->logged_in())
		{
			
			$this->view = View::factory('dashboard');
		}
		else
		{
			$form = array('email'=>'');
			$this->view = View::factory('index')
							->set('form', (object) $form);
		}
	}

}