<?php

class Vds_Controller_Auction extends Controller_Auth_Website {

	protected $form = array(
		'title'	=> '',
	);

	public function action_index()
	{
		$auction_id = $this->request->param('auction_id');
		$auction = Auction::instance(ORM::factory('Auction', $auction_id));
		$this->title = 'Dashboard';
		$this->view = View::factory('dashboard')
							->set('auction', $auction);
	}
	
	public function action_create()
	{
		$validation = Validation::factory($this->request->post())
								->rule('title', 'not_empty');
		
		if ($validation->check())
		{
			$auction = Auction::create()
								->creator($this->user)
								->title($this->request->post('title'))
								->save()
								->add_user($this->user)
								->save();

			$this->redirect('/dashboard');
		}

		$this->view = View::factory('auction/form')
								->set('form', (object) $this->form);
	}

}