<?php

class Vds_Contact extends Entity {

	public static function instance(Model_Contact $orm_contact)
	{
		return new Contact($orm_contact);
	}

	public static function create()
	{
		$orm_contact = ORM::factory('Contact');
		return new Contact($orm_contact);
	}

	public function __construct(Model_Contact $orm_contact)
	{
		$this->orm_object = $orm_contact;
	}

	public function name($name = NULL)
	{
		return $this->field_string('name', $name);
	}

}