<?php

class Vds_Item extends Entity {

	public static function create()
	{
		$orm_item = ORM::factory('Item');
		return new Item($orm_item);
	}

	public static function instance(Model_Item $orm_item)
	{
		return new Item($orm_item);
	}

	public function __construct(Model_Item $orm_item)
	{
		$this->orm_object = $orm_item;
	}

	public function name($name = NULL)
	{
		return $this->field_string('name', $name);
	}

	public function msrp($msrp = NULL)
	{
		return $this->field_string('msrp', $msrp);
	}

	public function value($value = NULL)
	{
		return $this->field_string('value', $value);
	}

	public function starting_bid($starting_bid = NULL)
	{
		return $this->field_string('starting_bid', $starting_bid);
	}

	public function reserve_price($reserve_price = NULL)
	{
		return $this->field_string('reserve_price', $reserve_price);
	}

	public function type($type = NULL)
	{
		return $this->field_string('type', $type);
	}

	public function auction(Auction $auction = NULL)
	{
		return $this->field_object('auction', $auction);
	}

	public function contact(Contact $contact = NULL)
	{
		return $this->field_object('contact', $contact);
	}

}