<?php

class Vds_Model_Auction extends ORM {

	protected $_belongs_to = array(
		'creator'	=> array(
			'model'			=> 'User',
			'foreign_key'	=> 'creator_id',
		),
	);

	protected $_has_many = array(
		'users'       => array(
			'model' 	=> 'User',
			'through' => 'auctions_users'
		),
		'items'       => array(),
	);

}