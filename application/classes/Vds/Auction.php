<?php

class Vds_Auction extends Entity {

	protected $items = array();

	public static function create()
	{
		$orm_auction = ORM::factory('Auction');
		return new Auction($orm_auction);
	}

	public static function instance(Model_Auction $orm_auction = NULL)
	{
		return new Auction($orm_auction);
	}

	public function __construct(Model_Auction $orm_auction)
	{
		$this->orm_object = $orm_auction;
	}

	public function creator(User $user = NULL)
	{
		if (isset($user))
		{
			$this->orm_object->creator_id = $user->id();
			return $this;
		}
		else
		{
			return User::factory($this->orm_object->creator);
		}
	}

	public function title($title = NULL)
	{
		return $this->field_string('title', $title);
	}

	public function add_user(User $user = NULL)
	{
		if ( ! $this->orm_object->has('users', $user->id()))
		{
			$this->orm_object->add('users', $user->id());
		}
		return $this;
	}

	public function items()
	{
		if (count($this->items) == 0)
		{
			$orm_items = $this->orm_object->items->find_all();
			foreach ($orm_items as $orm_item)
			{
				$this->items[] = Item::instance($orm_item);
			}
		}
		
		return $this->items;
	}

}