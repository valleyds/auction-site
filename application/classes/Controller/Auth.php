<?php

class Controller_Auth extends Vds_Controller_Auth {
	
	public function action_index()
	{
		$this->check_login();
		$form = array(
			'email'	=> '',
		);
		$post = $this->request->post();
		$error = NULL;
		if ( ! empty($post))
		{
			if (Auth::instance()->login($post['email'], $post['password']))
			{
				$user_orm = Auth::instance()->get_user();
				$this->user = User::instance($user_orm);
				if (Session::instance()->get('login_redirect'))
				{
					$this->redirect(Session::instance()->get('login_redirect'));
				} 
				else
				{
					$this->redirect('/');
				}
				
			} else {
				$form['email'] = $post['email'];
				$error = 'Invalid email/pass';
			}
		}

		$this->view = View::factory('auth/login')
							->set('form', (object) $form)
							->set('error', $error);
	}

}