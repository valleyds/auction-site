<?php

class User extends Vds_User {

	public function auctions()
	{
		if (count($this->auctions) == 0)
		{
			$orm_auctions = $this->orm_object->auctions->find_all();
			foreach ($orm_auctions as $orm_auction)
			{
				$this->auctions[] = Auction::instance($orm_auction);
			}
		}

		return $this->auctions;
	}

}