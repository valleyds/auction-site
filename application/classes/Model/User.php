<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {

	protected $_has_many = array(
		'user_tokens' => array('model' => 'User_Token'),
		'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
		'addresses'		=> array(
			'model'		=> 'Address',
			'through'	=> 'addresses_users',
		),
		'auctions'		=> array(
			'model'		=> 'Auction',
			'through'	=> 'auctions_users',
		),
	);

} // End User Model