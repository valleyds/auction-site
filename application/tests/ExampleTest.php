<?php

/**
 * Test case for Minion_Util
 *
 * @package    Kohana/Minion
 * @group      kohana
 * @group      kohana.minion
 * @category   Test
 * @author     Kohana Team
 * @copyright  (c) 2009-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 */

class ExampleTest extends Kohana_Unittest_TestCase
{

	public function string_provider()
	{
		return array(
			array(1,1),
			array(1,1),
		);
	}

	/**
	 * @dataProvider string_provider
	 * @return [type] [description]
	 */
	public function test_convert_task_to_class_name($first, $second)
	{
		$this->assertSame($first, $second);
	}

}
