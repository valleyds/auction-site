<div class="row">
	<div class="col-md-4 text-center">
		<div class="jumbotron">
			<h1>Contact</h1>
			<a href="#" class="btn btn-success btn-lg btn-block">Add Them</a>
		</div>
	</div>
	<div class="col-md-4 text-center">
		<div class="jumbotron">
			<h1>Item</h1>
			<a href="#" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modal-add-item">Add It</a>
			<?php echo View::factory('item/modal/add')->set('auction', $auction); ?>
		</div>
	</div>
	<div class="col-md-4 text-center">
		<div class="jumbotron">
			<h1>Need Flyer</h1>
			<a href="#" class="btn btn-success btn-lg btn-block">Print It</a>
		</div>
	</div>
</div>
<h2 class="sub-header">Auction Items</h2>
<div class="table-responsive">
	<table id="item_table" class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Item Number</th>
				<th>Item</th>
				<th>Value</th>
				<th>Donor</th>
				<th>Type</th>
			</tr>
		</thead>
		<tbody>
			<tr class="clone">
				<td class="item_id"></td>
				<td class="item_number">l1</td>
				<td class="item_name"></td>
				<td class="item_msrp"></td>
				<td class="item_donor"><?php echo HTML::anchor('donor/123', 'Jim Bob'); ?></td>
				<td class="item_type"></td>
			</tr>
			<?php foreach($auction->items() as $item) { ?>
				<tr id="item_row_<?php echo $item->id(); ?>">
					<td class="item_id"><?php echo $item->id(); ?></td>
					<td class="item_number">l1</td>
					<td class="item_name"><?php echo $item->name(); ?></td>
					<td class="item_msrp">$<?php echo $item->msrp(); ?></td>
					<td class="item_donor"><?php echo HTML::anchor('donor/123', $item->contact()->name()); ?></td>
					<td class="item_type"><?php echo $item->type(); ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>