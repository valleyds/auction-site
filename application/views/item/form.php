<?php echo Form::open('/item/add', array('id'=>'add-item-form', 'class' => 'form-horizontal')); ?>
	<?php echo Form::hidden('auction_id', $auction->id()); ?>
	<div class="form-group">
		<label for="item-name" class="col-sm-3">Item Name</label>
		<div class="col-sm-9">
			<input type="text" name="name" class="form-control" id="item-name" value="<?php echo isset($form->name) ? $form->name : ''; ?>" placeholder="Item Name">
		</div>
	</div>

	<div class="form-group">
		<label for="item-msrp" class="col-sm-3">Item MSRP</label>
		<div class="col-sm-4">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="msrp" class="form-control" id="item-msrp" value="<?php echo isset($form->msrp) ? $form->msrp : ''; ?>" placeholder="MSRP">
				<span class="input-group-addon">.00</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="item-value" class="col-sm-3">Item Value</label>
		<div class="col-sm-4">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="value" class="form-control" id="item-value" value="<?php echo isset($form->msrp) ? $form->msrp : ''; ?>" placeholder="value">
				<span class="input-group-addon">.00</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="item-starting_bid" class="col-sm-3">Starting Bid</label>
		<div class="col-sm-4">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="starting_bid" class="form-control" id="item-starting_bid" value="<?php echo isset($form->starting_bid) ? $form->starting_bid : ''; ?>" placeholder="Starting bid">
				<span class="input-group-addon">.00</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="item-reserve_price" class="col-sm-3">Reserve Price</label>
		<div class="col-sm-4">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="reserve_price" class="form-control" id="item-reserve_price" value="<?php echo isset($form->reserve_price) ? $form->reserve_price : ''; ?>" placeholder="Reserve Price">
				<span class="input-group-addon">.00</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="item-type" class="col-sm-3">Item Type</label>
		<div class="col-sm-3">
			<div class="radio">
				<label>
					<input type="radio" name="type" id="na" value="na" checked>
					Not Sure Yet
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="type" id="type" value="live">
					Live Auction
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="type" id="type" value="silent">
					Silent Auction
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="type" id="type" value="basket">
					Baskets
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="item-msrp" class="col-sm-3">Donor Name</label>
		<div class="col-sm-9">
			<input type="text" name="donor-name" class="form-control" id="donor-name" value="<?php echo isset($form->donor_name) ? $form->msrp : ''; ?>" placeholder="Donor Name">
		</div>
	</div>

	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<input type="submit" class="btn btn-primary" value="Save">

<?php echo Form::close(); ?>