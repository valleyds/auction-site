<?php if ($logged_in) { ?>
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="primary-nav">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Auctions <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<?php foreach ($user->auctions() as $auction) { ?>
								<li><a href="/auction/<?php echo $auction->id(); ?>"><?php echo $auction->title(); ?></a></li>
							<?php } ?>
							<li class="divider"></li>
							<li><a href="/auction/create">New Auction</a></li>
						</ul>
					</li>
					<li><a href="/items">Items</a></li>
					<li><a href="/contacts">Contacts</a></li>
					<li><a href="/sponsors">Sponsors</a></li>
					<li><a href="/print">Volunteers</a></li>
					<li><a href="/print">Food</a></li>
					<li><a href="/print">Bidders</a></li>
					<li><a href="/reports">Reports</a></li>
					<li><a href="/print">Print</a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Dashboard</a></li>
					<li><a href="/account">Profile</a></li>
					<li><a href="/logout">Sign out</a></li>
				</ul>

				<form action="/" class="navbar-form navbar-right">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="search items">
					</div>
					<input type="submit" class="btn btn-default" value="Search">
				</form>
			</div>
		</div>
	</nav>
<?php } ?>