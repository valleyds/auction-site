<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Auction</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

		
		<link rel="stylesheet" href="/css/main.css">
		<script src="/js/vendor/modernizr-2.7.1.min.js"></script>
		
	</head>
	<body>

		<!--[if lt IE 8]>
			<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<?php echo View::factory('element_menu'); ?>
		<!-- Add your site or application content here -->
		
		<section>
			<?php if ($logged_in) { ?>
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-3 col-md-2 sidebar">
							<h2 class="sub-heading">Items</h2>
							<ul class="nav nav-sidebar">
								<li><a href="#">Add Item</a></li>
								<li><a href="#">All Items</a></li>
								<li><a href="#">Baskets</a></li>
								<li><a href="#">Silent</a></li>
								<li><a href="#">Live</a></li>
							</ul>

							<h2 class="sub-heading">Contacts</h2>
							<ul class="nav nav-sidebar">
								<li><a href="#">Add Contacts</a></li>
								<li><a href="#">All Contacts</a></li>
								<li><a href="#">Donors</a></li>
								<li><a href="#">Follow Up</a></li>
								<li><a href="#">Invitations</a></li>
								<li><a href="#">Thanks</a></li>
							</ul>

							<h2 class="sub-heading">Print</h2>
							<ul class="nav nav-sidebar">
								<li><a href="#">Item Receipt</a></li>
								<li><a href="#">Request Letter</a></li>
								<li><a href="#">Item List</a></li>
								<li><a href="#">Volunteer List</a></li>
								<li><a href="#">Live Auction Items</a></li>
								<li><a href="#">Flyer</a></li>
								<li><a href="#">Silent Auction Sheets</a></li>
								<li><a href="#">Live Auction Item Numbers</a></li>
							</ul>
						</div>
						<div class="col-sm-9 col-md-10 main">
							<h1 class="page-header"><?php echo $page_title; ?></h1>
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			<?php } else { ?>
				<div class="container">
					<?php echo $content; ?>
				</div>
			<?php } ?>
		</section>
		
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						Powered By ValleyDS Website Design
					</div>
				</div>
			</div>
		</footer>
	
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>

		<script src="/js/plugins.js"></script>
		<script src="/js/main.js"></script>

		<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
		<script>
			(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
			function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
			e=o.createElement(i);r=o.getElementsByTagName(i)[0];
			e.src='//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
			ga('create','UA-XXXXX-X');ga('send','pageview');
		</script>
	</body>
</html>
