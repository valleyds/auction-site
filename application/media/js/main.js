var item = {
	init: function()
	{
		this.save_item();
	},
	save_item: function()
	{
		$('#add-item-form').validate({
			rules: {
				name: {
					required: true
				},
				msrp: {
					required: true,
					number: true
				}
			},
			submitHandler: function(form) {
				var action = $(form).attr('action');
				var data = $(form).serialize();
				$.post(action, data, function(data)
				{
					item_table.insert_row(data)
					$('#modal-add-item').modal('hide');
				}, 'json');

				return false;
			}
		});
	}
};
var item_table = {
	init: function()
	{

	},
	insert_row: function(item)
	{
		var table = $('#item_table');
		var new_row = $('.clone', table).clone().removeClass('clone').attr('id', 'item_row_'+item.id);
		$('.item_id', new_row).html(item.id);
		$('.item_number', new_row).html(item.item_number);
		$('.item_name', new_row).html(item.name);
		$('.item_msrp', new_row).html(item.msrp);
		$('.item_donor', new_row).html(item.contact.name);
		$('.item_type', new_row).html(item.type);
		table.prepend(new_row);
	}
};

$(document).ready(function(){
	
	item.init();

	item_table.init();

});