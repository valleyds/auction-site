(function(){
	var app = angular.module('store', []);

	app.controller('StoreController', function(){
		this.products = gems;
	});

	var gems = [
		{
			name: "boba cabana",
			price: 2,
			description: '...',
			canPurchase: true,
			soldOut: false,
			images: [
				{
					full: 'file.png',
					thumb: 'file.png'
				}
			]
		},
		{
			name: "boba cabanaasdf",
			price: 2.95,
			description: '...',
			canPurchase: false,
			soldOut: false
		}
	];

	app.controller('PanelController', function(){
		this.tab = 1;
		this.selectTab = function(setTab) {
			this.tab = setTab;
		};
		this.isSelected = function(checkTab)
		{
			return this.tab === checkTab;
		}
	});

})();
